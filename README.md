# Audsley Scheduler

> This project was designed for INFOF404 course at ULB (Université Libre de Bruxelles).

This project aims to implement Audsley algorithm for uniprocessor FTP scheduling.

## Run the code

### Install dependencies
If it is your first time running the code, run to install dependencies:
```bash
pip3 install -r requirements.txt
```
To run the code:
```bash
python3 project.py audsley|scheduler <task_file>
```
If `scheduler` is used, the output graphic could be found in `output` directory under the name of `ftpFig.jpg`.