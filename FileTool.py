from FileTool import *


class FileTool:
    """A utility class to manipulate files"""

    @staticmethod
    def read(file_path):
        """Opens the file and stores its data line by line in a list

        Args:
            file_path (str): File path

        Returns:
            [list]: A list containing file data line by line
        """
        with open(file_path) as file:
            return file.read().splitlines()

    @staticmethod
    def write(file_name, text):
        """Writes the given text to the given file path

        Args:
            file_name ([str]): File path
            text ([str]): Content to write to the file
        """
        with open(file_name, "w") as f:
            f.write(text)
