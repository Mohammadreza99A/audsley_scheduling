from Task import *
from math import gcd

class OverloadSystemException(Exception):
    """Exception class for overloaded system"""
    pass


class FTPScheduler:
    """Class scheduling a set of tasks"""

    def __init__(self, tasks):
        """Constructor

        Args:
            tasks (Task[]): List of tasks
        """

        self.tasks = tasks

        if (not self.check_utilization_condition()):
            raise OverloadSystemException("Overloaded System. Utilization is " +
                            str(self.calculate_utilization()))

        self.max_offset = self.find_max_offset()
        self.hyper_period = self.get_hyper_period()

        # Represents the timeline
        self.time = [None for _ in range(
            self.max_offset + (2 * self.hyper_period))]

        # Number of current job
        self.jobNm = [-1 for _ in range(len(self.tasks))]

        # Absolute deadlines of tasks
        self.task_deadlines = [[] for _ in range(len(self.tasks))]
        self.init_deadlines()

        # Release time of jobs for tasks
        self.release_time_tasks = [[] for _ in range(len(self.tasks))]
        self.init_release_time()
        self.missed_deadline = None  # Missed deadline ([tasks], time)

        # Computation requirement of jobs
        self.wcets = [0 for _ in range(len(self.tasks))]

    def calculate_utilization(self):
        """Calculates utilization of the system

        Returns:
            [int]: Utilization
        """
        u = 0
        for task in self.tasks:
            u += task.utilization()

        return u

    def check_utilization_condition(self):
        """Checks the necessary condition (U(t) <= 1) of the system

        Returns:
            [bool]: True if necessary condition is met and False otherwise
        """
        if (self.calculate_utilization() > 1):
            return False
        return True

    def find_max_offset(self):
        """Returns the max offset of tasks

        Returns:
            [int]: Max offset
        """
        max_offset = 0
        for task in self.tasks:
            if task.offset > max_offset:
                max_offset = task.offset
        return max_offset

    def init_deadlines(self):
        """Initiate deadline list"""

        for taksInd in range(len(self.tasks)):
            jobNb = 0
            t = self.tasks[taksInd].offset + \
                (jobNb * self.tasks[taksInd].period) + \
                self.tasks[taksInd].deadline

            while t <= len(self.time):
                self.task_deadlines[taksInd].append(t)
                jobNb += 1
                t = self.tasks[taksInd].offset + \
                    (jobNb * self.tasks[taksInd].period) + \
                    self.tasks[taksInd].deadline

    def init_release_time(self):
        """Initiate next release time of tasks"""

        for taksInd in range(len(self.tasks)):
            jobNb = 0
            t = self.tasks[taksInd].offset + \
                (jobNb * self.tasks[taksInd].period)
            while t <= len(self.time):
                self.release_time_tasks[taksInd].append(t)
                jobNb += 1
                t = self.tasks[taksInd].offset + \
                    (jobNb * self.tasks[taksInd].period)

    def schedule(self, is_hard):
        """This method does the hard/soft FTP scheduling.
        This method considers that the given task set is ordered from the highest priority to the lowest priority task.

        Args:
            is_hard (bool): Indicates if it's hard FTP scheduling or soft

        Returns:
            [bool]: is schedulable or not
        """

        t = 0
        while t < len(self.time):
            if (not self.miss_deadline_acceptable(t, is_hard)):
                return False

            self.update_computation_requirement(t)
            taskInd = self.give_task_index_to_execute()

            if taskInd != None:
                self.time[t] = self.tasks[taskInd].name
                self.wcets[taskInd] -= 1
            t += 1

        return True

    def update_computation_requirement(self, t):
        """This method updates computation requirement when it's a new release time

        Args:
            t (int): Time in the timeline
        """

        for taskInd in range(len(self.tasks)):
            jobNb = self.jobNm[taskInd] + 1  # release time of next job
            release_time_task = self.release_time_tasks[taskInd]
            if (jobNb < len(release_time_task) and t == release_time_task[jobNb]):
                self.wcets[taskInd] += self.tasks[taskInd].wcet
                self.jobNm[taskInd] = jobNb

    def miss_deadline(self, t):
        """
        This method returns the task missed its deadline at time t
        and has lower priority than other tasks missed their deadline
        Args:
            t (int): Time in the timeline 

        Returns:
            [int[]]: List of index of tasks that have missed their deadline at time t
        """
        task_ind_missed=[]
        for taskInd in range(len(self.tasks) - 1, -1, -1):
            jobNb = self.jobNm[taskInd]
            deadline = self.task_deadlines[taskInd]
            if (self.wcets[taskInd] > 0 and jobNb < len(deadline) and t >= deadline[jobNb]):
                task_ind_missed.append(taskInd)
        return task_ind_missed

    def miss_deadline_acceptable(self, t, is_hard):
        """
        This method verifies if the task that missed its deadline is acceptable regarding to
        hard/soft scheduling
        Args:
            t (int): Time in the timeline
            is_hard (bool): Indicates if it's hard FTP scheduling or soft
        Returns:
            [bool]: True if tasks missed their deadline is acceptable, False otherwise"""
        tasksInd = self.miss_deadline(t)
        if (len(tasksInd)==0 or (not is_hard and tasksInd[0] != len(self.tasks) - 1)):
            return True
        self.missed_deadline = (tasksInd, t)
        return False

    def give_task_index_to_execute(self):
        """This method returns index of task in tasks that need to be executed first, None means the CPU is idle at time t

        Args:
            t (int): Time in the timeline

        Returns:
            [int | None]: Index of task or None if CPU is idle
        """

        for i in range(len(self.tasks)):
            if (self.wcets[i] > 0):
                return i
        return None

    def get_hyper_period(self):
        """Returns the hyper period given the set of tasks

        Args:
            tasks (Task[]): Set of tasks

        Returns:
            [int]: Hyper period
        """
        # Getting the period (T) of each task
        periods = [task.period for task in self.tasks]
        return self.lcm(periods)

    def lcm(self, numArray):
        """Calculates the least common multiplier from the given array of numbers

        Args:
            numArray (int[]): Array of integers

        Returns:
            [int]: Least common multiplier of the array
        """
        lcm = 1
        for i in numArray:
            lcm = lcm * i // gcd(lcm, i)
        return lcm

    def __str__(self):
        output = ""

        for i in range(0, len(self.time)):
            if self.time[i] is not None:
                s = "time {0}-{1}: {2} \n".format(str(i),
                                                  str(i + 1), self.time[i])
                output = output + s
            else:
                s = "time {0}-{1}: IDLE \n".format(str(i), str(i + 1))
                output = output + s

        return output
