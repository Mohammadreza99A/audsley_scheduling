import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np


class FTPSchedulerGraphic:
    """Class that outputs the FTPScheduler class's output into a graphic"""

    def __init__(self,  scheduler):
        """Constructor

        Args:
            tasks (Task[]): List of tasks
            scheduler (FTPScheduler): Timeline list
        """
        self.tasks = scheduler.tasks
        self.time = scheduler.time  # Represents timeline
        self.deadlines = scheduler.task_deadlines  # Represents tasks deadlines
        self.releases = scheduler.release_time_tasks  # Represents release time of jobs
        self.missed_deadline = scheduler.missed_deadline  # Represents the missed deadline

        self.task_names = []

        for task in self.tasks:
            self.task_names.append("Task " + str(task.name))
        self.task_names.reverse()

    def plot_timeline(self):
        """Plots the timeline and save it to a file in output directory"""

        mat = [[0 for _ in range(len(self.time))]
               for _ in range(len(self.tasks))]

        for i in range(len(self.time)):
            elem = self.time[i]
            if elem is not None:
                mat[elem - 1][i] = elem

        mat.reverse()

        _, ax = plt.subplots(figsize=(18, 9))

        ax.set_yticks(range(1, len(mat)+1))
        ax.set_yticklabels(tuple(self.task_names))
        ax.invert_yaxis()
        ax.set_ylabel("Tasks")

        x_interval = 1
        if (len(self.time) > 50):
            x_interval = 5
        if (len(self.time) > 500):
            x_interval = 10

        ax.set_xbound(len(self.time))
        ax.set_xticks(np.arange(0, len(self.time), x_interval))
        ax.set_xlabel("Time")

        # Setting x axis label ticks interval
        for n, label in enumerate(ax.xaxis.get_ticklabels()):
            if n % x_interval != 0:
                label.set_visible(False)

        # Plotting the timeline
        for i in range(len(mat)):
            color = np.random.rand(3,)
            for j in range(len(mat[i])):
                if mat[i][j] != 0:
                    x = [j, j]
                    x2 = [j+1, j+1]
                    y = [i, i+1]
                    plt.fill_betweenx(y, x, x2, color=color)

        # Marking deadlines
        self.deadlines.reverse()
        for i in range(len(self.deadlines)):
            for j in range(len(self.deadlines[i])):
                x = [self.deadlines[i][j], self.deadlines[i][j]]
                y = [i, i+1]
                plt.plot(x, y, 'k')
                plt.plot(x, [i+1, i+1], 'vk')

        # Marking releasetimes
        self.releases.reverse()
        for i in range(len(self.releases)):
            for j in range(len(self.releases[i])):
                x = [self.releases[i][j], self.releases[i][j]]
                plt.plot(x, [i, i], 'ob')

        if self.missed_deadline != None:
            for taskInd in self.missed_deadline[0]:
                y = abs(taskInd - (len(self.tasks)-1))
                plt.plot([self.missed_deadline[1], self.missed_deadline[1]], [
                    y, y + 1], "r", linewidth=5, markersize=12)
                plt.plot([self.missed_deadline[1], self.missed_deadline[1]], [
                    y, y], "rx", linewidth=5, markersize=12)

        # Drawing the legend
        legend_elements = [
            Line2D([0], [0], color='k', lw=2, label='Absolute Deadline'),
            Line2D([0], [0], marker='o', color='w', label='Release Time',
                   markerfacecolor='b', markersize=10),
            Line2D([0], [0], marker='X', color='w', label='Missed Deadline',
                   markerfacecolor='r', markersize=10)]
        plt.legend(handles=legend_elements, loc=1)

        plt.grid()
        plt.savefig("output/ftpFig.jpg")
