class Task:
    """Represents a CPU task"""

    def __init__(self, offset, wcet, deadline, period, name):
        """Constructor

        Args:
            offset (int): Task offset
            wcet (int): Worst case execution time of task
            deadline (int): Task deadline
            period (int): Task period
            name (int): Task name
        """

        self.offset = offset  # O
        self.wcet = wcet  # C
        self.deadline = deadline  # D
        self.period = period  # T
        self.name = name

    def utilization(self):
        """Calculates the utilization of the task

        Returns:
            float: Utilization of the task
        """
        u = self.wcet / self.period
        return u

    def print(self):
        return str(self.offset) + ' ' + str(self.wcet) + ' ' + str(self.deadline) + ' ' + str(self.period)

    def __repr__(self):
        return '{' + str(self.offset) + ', ' + str(self.wcet) + ', ' + str(self.deadline) + ', ' + str(self.period) + ', ' + str(self.name) + '}'

    def __str__(self):
        return "Name: " + str(self.name) + " | " + "O: " + str(self.offset) + " | " + "C: " + str(self.wcet) + " | " + "D: " + str(self.deadline) + " | " + "T: " + str(self.period)
