import sys
from copy import deepcopy
from FileTool import *
from Task import *
from FTPScheduler import *
from FTPSchedulerGraphic import *
from Audsley import *

if __name__ == '__main__':
    if(len(sys.argv) != 3):
        print("Bad arguments: ./project audsley|scheduler <task_file>")
        sys.exit(1)

    option = sys.argv[1]
    task_file_path = sys.argv[2]

    tasks = []

    # Reading the task file and creating task objects
    file_lines = FileTool.read(task_file_path)
    for i in range(len(file_lines)):
        line = file_lines[i].split()
        task = Task(int(line[0]), int(line[1]), int(
            line[2]), int(line[3]), i+1)
        tasks.append(task)
    try:
        if option == "audsley":
            audsley = Audsley(tasks)
            res = audsley.run_audsley()

            if (not res):
                print("Schedule is not feasible!!!")
            else:
                print(
                    "Schedule is feasible! Wrote the output to audsley.txt file in output directory")

        elif option == "scheduler":
            # Sorting tasks from highest to lowest priority in a new list
            sorted_tasks = deepcopy(tasks)
            #sorted_tasks.sort(key=lambda x: x.period)
            scheduler = FTPScheduler(sorted_tasks)
            res = scheduler.schedule(True)

            if(not res):
                print("Schedule is not feasible!!!")
                print(scheduler)
            else:
                print(scheduler)

            graphic = FTPSchedulerGraphic(scheduler)
            graphic.plot_timeline()
        else:
            print("Argument not known.")
            sys.exit(1)
    except OverloadSystemException as err:
        print(err.args[0])
    except Exception as err:
        print(err.args)
