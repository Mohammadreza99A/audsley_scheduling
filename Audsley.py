from FTPScheduler import *
from FileTool import *


class Audsley:
    """Implements the Audsley algorithm for a set of tasks"""

    def __init__(self, tasks):
        """Constructor

        Args:
            tasks (Task[]): List of tasks
        """
        self.tasks = tasks
        self.file_name = "./output/audsley.txt"

    def audsley(self):
        """
        Assign priorities and return whether a feasible FTP assignment has been
        found or not.

        Returns:
            [bool]: Feasible FTP found or not
        """

        tasks = self.tasks

        if (len(tasks) <= 1):
            return True

        for i in range(len(tasks)):
            # determine tasks[i] as lowest-priority task
            tasks.insert(len(tasks) - 1, tasks.pop(i))
            scheduler = FTPScheduler(tasks)

            if (scheduler.schedule(False)):
                # Apply recursively the same technique to the subset tasks\{tasks[i]} of cardinality n–1
                lpt = tasks.pop(-1)

                if self.audsley():
                    tasks.append(lpt)
                    return True

                tasks.append(lpt)
            # return to the initial order
            tasks.insert(i, tasks.pop(-1))
        return False

    def run_audsley(self):
        """This method execute audsley algorithm and
        if a FTP assignment has been found, then the task file is created

        Returns:
            [bool]: True if it is schedulable and False otherwise
        """

        if (self.audsley()):
            self.write_assignment()
            return True
        return False

    def write_assignment(self):
        """Create and write in task file"""

        text = ""

        for task in self.tasks:
            text += task.print() + '\n'

        text = text[:-1]
        FileTool.write(self.file_name, text)
